
# Install WxWidgets 


## Follow the installWidgets.pdf guide
- see docs/
- it will take 60 minuttes or so to compile
- see perhaps history.md for a (likely incomplete) set of commands.

### Its was along these lines:
- goto https://wxwidgets.org/downloads/
- download the source code (mac/linux etc)
- from https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.2/wxWidgets-3.1.2.tar.bz2
- expand files to your home directory, create as bin folder, and save with the version number like ```"/home/larsk/bin/wxWidgets-3.1.2"```

```bash
sudo apt-get install build-essential
sudo apt-get install libwxgtk3.0-dev
```
-> AND  follow the instructions under installwxWidgets.pdf
 (this will take quite a while 30-60minss). Copy the expanded


## compile (and run)
- from https://docs.wxwidgets.org/3.0/overview_helloworld.html

```bash
g++ src/simple.cpp `wx-config --cxxflags --libs std` -o bin/simple && bin/simple

g++ src/hello.cpp  `wx-config --cxxflags --libs std` -o bin/hello && bin/hello
```


c_cpp_properties.json (change '/home/larsk/' to your own envoironment)
```json

{
    "configurations": [
        {
            "name": "Linux",
            "includePath": [
                "${workspaceFolder}/**",
                "/home/larsk/bin/wxWidgets-3.1.2/include/",
                   "/home/larsk/bin/wxWidgets-3.1.2/gtk-build/lib/wx/include/gtk3-unicode-3.1"
            ],
            "defines": [],
            "compilerPath": "/usr/bin/gcc",
            "cStandard": "c11",
            "cppStandard": "c++17",
            "intelliSenseMode": "clang-x64"
        }
    ],
    "version": 4
}
```
